function diemKhuVuc(khuVuc) {
  var khuVuc = document.getElementById("txtkhuvuc").value * 1;
  var diemUuTienKhuVuc = null;
  switch (khuVuc) {
    case 0: {
      diemUuTienKhuVuc = 0;
      return diemUuTienKhuVuc;
    }
    case 1: {
      diemUuTienKhuVuc = 2;
      return diemUuTienKhuVuc;
    }
    case 2: {
      diemUuTienKhuVuc = 1;
      return diemUuTienKhuVuc;
    }
    case 3: {
      diemUuTienKhuVuc = 0.5;
      return diemUuTienKhuVuc;
    }
  }
}

function diemDoiTuong(doiTuong) {
  var doiTuong = document.getElementById("txt-doi-tuong").value * 1;
  var diemUuTienDoiTuong = null;
  switch (doiTuong) {
    case 0: {
      diemUuTienDoiTuong = 0;
      return diemUuTienDoiTuong;
    }
    case 1: {
      diemUuTienDoiTuong = 2.5;
      return diemUuTienDoiTuong;
    }
    case 2: {
      diemUuTienDoiTuong = 1.5;
      return diemUuTienDoiTuong;
    }
    case 3: {
      diemUuTienDoiTuong = 1;
      return diemUuTienDoiTuong;
    }
  }
}

function tinhDiem() {
  var diemUuTienKhuVucValue = diemKhuVuc();
  console.log(diemUuTienKhuVucValue);
  var diemUuTienDoiTuong = diemDoiTuong();
  console.log(diemUuTienDoiTuong);
  var diemMonDauTien = document.getElementById("txt-diem-thu-nhat").value * 1;
  var diemMonThuHai = document.getElementById("txt-diem-thu-hai").value * 1;
  var diemMonCuoiCung = document.getElementById("txt-diem-thu-ba").value * 1;
  var diemTrungBinh =
    diemMonDauTien +
    diemMonThuHai +
    diemMonCuoiCung +
    diemUuTienDoiTuong +
    diemUuTienKhuVucValue;
  console.log(diemTrungBinh);
  var diemChuan = document.getElementById("txt-diem-chuan");
  if (diemChuan <= diemTrungBinh) {
    document.getElementById(
      "result"
    ).innerHTML = `Điểm trung bình là ${diemTrungBinh} bạn đã trượt`;
  } else {
    document.getElementById(
      "result"
    ).innerHTML = `Điểm trung bình là ${diemTrungBinh} bạn đã đỗ`;
  }
}
