function tinhTien(soKw) {
  //   ten = document.getElementById("txt-khach-hang");
  soKw = document.getElementById("txt-kw").value * 1;
  var tienDien = null;
  if (soKw < 50) {
    tienDien = soKw * 500;
    return tienDien;
  }
  if (50 <= soKw && soKw <= 100) {
    tienDien = 50 * 500 + (soKw - 50) * 650;
    return tienDien;
  }
  if (100 <= soKw && soKw <= 200) {
    tienDien = 50 * 500 + 50 * 650 + (soKw - 50 - 50) * 850;
    return tienDien;
  }
  if (150 <= soKw && soKw <= 350) {
    tienDien = 50 * 500 + 50 * 650 + 100 * 850 + (soKw - 50 - 50 - 100) * 1300;
    return tienDien;
  }
}

function hoaDon() {
  var tienDienValue = tinhTien();
  console.log(tienDienValue);
  var ten = document.getElementById("txt-khach-hang");
  document.getElementById(
    "result"
  ).innerHTML = `Khách hàng ${ten} phải đóng ${tienDienValue}VND tiền điện`;
}
